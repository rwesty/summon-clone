import { FunctionComponent, useState } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { AppState } from '@/store';

import Calendar from './calendar/Calendar';
import CalendarHeader from './calendar/CalendarHeader';
import CalendarToolbar from './calendar/CalendarToolbar';
import CreateTimeSlotDialog from './CreateTimeSlotDialog';

type StartEndType = {
  startTime: Date;
  endTime: Date;
};

const eventPropGetter = (event) =>
  event.resource
    ? {
        className: `high-priority`,
      }
    : {};

const CalendarView: FunctionComponent = () => {
  const { timeSlots } = useSelector((state: AppState) => state);
  const [createSlot, setCreateSlot] = useState<StartEndType | false>(false);

  return (
    <Wrapper>
      {createSlot && (
        <CreateTimeSlotDialog
          startTime={createSlot.startTime}
          endTime={createSlot.endTime}
          onDismiss={() => setCreateSlot(false)}
        />
      )}
      <Calendar
        onSelectSlot={(slotInfo) =>
          setCreateSlot({
            startTime: new Date(slotInfo.start),
            endTime: new Date(slotInfo.end),
          })
        }
        eventPropGetter={eventPropGetter}
        events={timeSlots.map((slot) => ({
          start: new Date(slot.startTime),
          end: new Date(slot.endTime),
          resource: slot.highPriority,
        }))}
        components={{
          toolbar: CalendarToolbar,
          header: CalendarHeader,
        }}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: relative;
  flex: 3;
  margin-right: 30px;

  border-radius: 20px;
  background: #3d3c40;
  padding: 30px;

  @media (max-width: 1024px) {
    margin-right: 0;
    margin-bottom: 30px;
  }
`;

export default CalendarView;
