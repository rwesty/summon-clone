import { FunctionComponent } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';

import { User } from '@/models/types';
import { AppState } from '@/store';
import { useStoreActions } from '@/store/actions';

import FormDialog from '@/components/FormDialog';
import Button from '@/components/shared/Button';
import Input from '@/components/shared/Input';
import InputWrapper from '@/components/shared/InputWrapper';
import InputError from '@/components/shared/InputError';
import Checkbox from '@/components/shared/Checkbox';
import UserMenuButton from '@/components/UserMenuButton';
import Label from '@/components/shared/Label';

type Props = {
  users: User[];
  onDismiss: () => void;
};
const CreateRequestDialog: FunctionComponent<Props> = ({
  users,
  onDismiss,
  ...rest
}) => {
  const dispatch = useDispatch();
  const storeActions = useStoreActions(dispatch);
  const { user } = useSelector((state: AppState) => state);
  const { register, handleSubmit, setValue, errors } = useForm();

  register(`invites`);

  const onSubmit = async (data) => {
    await storeActions.createRequest({
      creator: user.id,
      title: data.title,
      description: data.notes,
      highPriority: data.highPriority,
      invites: data.invites.map((invite) => ({
        id: invite.id,
      })),
    });

    onDismiss();
  };

  return (
    <FormDialog
      title="Create Task"
      onDismiss={onDismiss}
      content={
        <form autoComplete="off">
          <InputWrapper>
            {errors.title?.type === `required` && (
              <InputError>Title is required.</InputError>
            )}
            {errors.title?.type === `maxLength` && (
              <InputError>Character limit reached.</InputError>
            )}
            <Input
              name="title"
              placeholder="Title"
              ref={register({ required: true, maxLength: 50 })}
            />
          </InputWrapper>
          <TextareaWrapper>
            {errors.notes && <InputError>Character limit reached.</InputError>}
            <Textarea
              name="notes"
              placeholder="Notes"
              ref={register({ maxLength: 512 })}
            />
          </TextareaWrapper>
          <Checkbox
            id="highPriority"
            label="High priority"
            subLabel="Invitees set high priority hours when they are typically busy"
            name="highPriority"
            ref={register}
          />
          <Label>Invite members</Label>
          <UserMenuButton
            users={users}
            onChange={(selectedUsers) => setValue(`invites`, selectedUsers)}
          />
        </form>
      }
      actions={
        <>
          <CancelButton onClick={onDismiss}>Cancel</CancelButton>
          <CreateButton onClick={handleSubmit(onSubmit)}>Create</CreateButton>
        </>
      }
      {...rest}
    />
  );
};

const CancelButton = styled(Button)`
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
  box-shadow: none;
  color: rgba(255, 255, 255, 0.5);
  margin-right: 15px;
`;

const CreateButton = styled(Button)`
  background: #2dadf6;
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
`;

const TextareaWrapper = styled(InputWrapper)`
  height: 80px;
`;

const Textarea = styled.textarea`
  width: 100%;
  height: 80px;
  resize: none;
  background: none;
  border: none;
  font-family: 'Neue Haas Unica';
  padding: 10px;
  font-size: 16px;
  color: white;

  ::placeholder {
    font-size: 20px;
    color: rgba(255, 255, 255, 0.5);
  }
`;

export default CreateRequestDialog;
