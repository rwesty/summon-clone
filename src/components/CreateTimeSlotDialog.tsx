import { FunctionComponent } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { format } from 'date-fns';

import { AppState } from '@/store';
import { useStoreActions } from '@/store/actions';

import FormDialog from '@/components/FormDialog';
import Button from '@/components/shared/Button';
import Checkbox from '@/components/shared/Checkbox';
import Label from '@/components/shared/Label';

type Props = {
  startTime: Date;
  endTime: Date;
  onDismiss: () => void;
};
const CreateTimeSlotDialog: FunctionComponent<Props> = ({
  startTime,
  endTime,
  onDismiss,
  ...rest
}) => {
  const dispatch = useDispatch();
  const storeActions = useStoreActions(dispatch);
  const { user } = useSelector((state: AppState) => state);

  const { register, handleSubmit } = useForm();

  const onSubmit = async (data) => {
    await storeActions.createTimeSlot({
      user: user.id,
      startTime,
      endTime,
      highPriority: data.highPriority,
    });

    onDismiss();
  };

  return (
    <FormDialog
      title="Create Time Slot"
      onDismiss={onDismiss}
      content={
        <form autoComplete="off">
          <Label>From</Label>
          <Date>{format(startTime, `MMM Do 'at' ha`)}</Date>
          <Label>To</Label>
          <Date>{format(endTime, `MMM Do 'at' ha`)}</Date>
          <Checkbox
            id="highPriority"
            label="High priority only"
            subLabel="You will only be interrupted by high priority tasks"
            name="highPriority"
            ref={register}
          />
        </form>
      }
      actions={
        <>
          <CancelButton onClick={onDismiss}>Cancel</CancelButton>
          <CreateButton onClick={handleSubmit(onSubmit)}>Create</CreateButton>
        </>
      }
      {...rest}
    />
  );
};

const CancelButton = styled(Button)`
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
  box-shadow: none;
  color: rgba(255, 255, 255, 0.5);
  margin-right: 15px;
`;

const CreateButton = styled(Button)`
  background: #2dadf6;
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
`;

const Date = styled.div`
  color: rgba(255, 255, 255, 0.5);
  margin-bottom: 15px;
`;

export default CreateTimeSlotDialog;
