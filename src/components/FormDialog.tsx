import { FunctionComponent } from 'react';
import styled from 'styled-components';

import Dialog from '@/components/shared/Dialog';
import DialogTitle from '@/components/shared/DialogTitle';
import { DialogProps } from '@reach/dialog';

type Props = {
  title: string;
  content: React.ReactNode;
  actions: React.ReactNode;
} & DialogProps;

const FormDialog: FunctionComponent<Props> = ({
  title,
  content,
  actions,
  ...rest
}) => (
  <StyledDialog aria-label={title} {...rest}>
    <Wrapper>
      <DialogTitle>{title}</DialogTitle>
      {content}
    </Wrapper>
    <DialogActions>{actions}</DialogActions>
  </StyledDialog>
);

const StyledDialog = styled(Dialog)`
  @media (max-width: 480px) {
    &[data-reach-dialog-content] {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
    }
  }
`;

const Wrapper = styled.div`
  padding: 30px;
`;

const DialogActions = styled.div`
  padding: 30px;
  background: #313033;
  padding: 20px;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  display: flex;
  justify-content: flex-end;
  align-self: flex-end;

  @media (max-width: 480px) {
    border-radius: 0;
    width: 100%;
  }
`;

export default FormDialog;
