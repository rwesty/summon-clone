import { FunctionComponent, useState } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { saveSessionResponse } from '@/services/request';
import { AppState, RequestWithResponse } from '@/store/reducers';

import FormDialog from '@/components/FormDialog';
import Button from '@/components/shared/Button';
import Label from '@/components/shared/Label';
import Avatar from '@/components/shared/Avatar';

type Props = {
  request: RequestWithResponse;
  onDismiss: () => Promise<void>;
};
const StartSessionDialog: FunctionComponent<Props> = ({
  request,
  onDismiss,
  ...rest
}) => {
  const { user, users } = useSelector((state: AppState) => state);
  const [launched, setLaunched] = useState(false);
  const { invites } = request;

  const creator =
    request.userId === user.id
      ? user
      : users.find((u) => u.id === request.userId);

  const queue = [
    {
      ...creator,
      response: `joined`,
    },
    ...invites.filter((u) => u.id !== request.userId),
  ];

  return (
    <FormDialog
      title="You are being requested to join a session"
      onDismiss={onDismiss}
      content={
        <>
          {launched && <Launch src="/img/launch.gif" alt="" />}
          <StyledLabel>See member responses below.</StyledLabel>
          <StyledLabel>Title</StyledLabel>
          <Data>{request.title}</Data>
          <StyledLabel>Notes</StyledLabel>
          <Data>{request.description}</Data>
          <StyledLabel>Members</StyledLabel>
          <MemberList>
            {queue.map((member) => (
              <Member>
                {member.id === request.userId ||
                member.response === `joined` ? (
                  <Check className="fa fa-check" />
                ) : member.response === `denied` ? (
                  <Cross className="fa fa-times" />
                ) : null}
                <Avatar src={`/img/${member.icon}`} />
              </Member>
            ))}
          </MemberList>
        </>
      }
      actions={
        <>
          <CancelButton onClick={() => onDismiss()}>
            I&apos;m unavailable
          </CancelButton>
          <CreateButton
            onClick={() => {
              setLaunched(true);
              saveSessionResponse(user.id, request.id, `joined`);
            }}
          >
            Accept
          </CreateButton>
        </>
      }
      {...rest}
    />
  );
};

const CancelButton = styled(Button)`
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
  box-shadow: none;
  color: rgba(255, 255, 255, 0.5);
  margin-right: 15px;
`;

const CreateButton = styled(Button)`
  background: #2dadf6;
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
`;

const StyledLabel = styled(Label)`
  color: rgba(255, 255, 255, 0.5);
  font-size: 16px;
  margin-bottom: 5px;
`;

const Data = styled.div`
  font-size: 18px;
`;

const MemberList = styled.div`
  display: flex;
  margin-top: 15px;
`;

const Member = styled.div<{ joined: boolean; denied: boolean }>`
  position: relative;
  width: fit-content;
  margin-right: 10px;
`;

const Check = styled.i`
  position: absolute;
  top: -5px;
  right: -5px;
  color: #57d6be;
  font-size: 20px;
  z-index: 100;
`;

const Cross = styled.i`
  position: absolute;
  top: -5px;
  right: -5px;
  color: #f9381c;
  font-size: 20px;
  z-index: 100;
`;

const Launch = styled.img`
  position: absolute;
  top: 10%;
  left: 50%;
  transform: translate(-50%, 0%);
  z-index: 1000;
`;

export default StartSessionDialog;
