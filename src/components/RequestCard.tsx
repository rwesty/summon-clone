import { FunctionComponent, useState } from 'react';
import styled from 'styled-components';
import { format } from 'date-fns';
import { useSelector } from 'react-redux';

import { AppState, RequestWithResponse } from '@/store/reducers';
import { saveSessionResponse, startSession } from '@/services/request';

import Avatar from '@/components/shared/Avatar';
import Button from './shared/Button';
import StartSessionDialog from './StartSessionDialog';
import JoinSessionDialog from './JoinSessionDialog';

type Props = {
  request: RequestWithResponse;
};
const Requests: FunctionComponent<Props> = ({ request }) => {
  const { user, users } = useSelector((state: AppState) => state);
  const [showStartSessionDialog, setShowStartSessionDialog] = useState(false);
  const [requestDenied, setRequestDenied] = useState(false);

  const creator =
    request.userId === user.id
      ? user
      : users.find((u) => u.id === request.userId);

  const invites = [creator, ...request.invites];

  return (
    <Wrapper highPriority={request.highPriority} canStart={request.canStart}>
      {showStartSessionDialog && (
        <StartSessionDialog
          request={request}
          onDismiss={() => setShowStartSessionDialog(false)}
        />
      )}
      {request.started && !requestDenied && (
        <JoinSessionDialog
          request={request}
          onDismiss={async () => {
            await saveSessionResponse(user.id, request.id, `denied`);
            setRequestDenied(true);
          }}
        />
      )}

      <Title>{request.title}</Title>
      {request.description && <Description>{request.description}</Description>}

      <CardBody>
        <Invites>
          {invites.map((invite, i) => (
            <Avatar
              stacked
              z={i}
              key={invite.id}
              src={`/img/${invite.icon}`}
              alt=""
            />
          ))}
        </Invites>
        <div>
          {request.canStart ? (
            <>
              <Next>Available now</Next>
              <StartButton
                onClick={async () => {
                  if (request.started) {
                    setRequestDenied(false);
                  } else {
                    await startSession(user.id, request.id);
                    setShowStartSessionDialog(true);
                  }
                }}
              >
                {request.started ? `Join` : `Start`}
              </StartButton>
            </>
          ) : (
            <>
              <Next>Next available</Next>
              {request.nextAvailableStartTime
                ? format(
                    new Date(request.nextAvailableStartTime),
                    `MMM Do 'at' ha`,
                  )
                : `N/A`}
            </>
          )}
        </div>
      </CardBody>
    </Wrapper>
  );
};

const Wrapper = styled.div<{ highPriority: boolean; canStart: boolean }>`
  border-radius: 0 20px 20px 0;
  background: #555459;
  padding: 20px;
  box-shadow: 1px 2px 4px -2px black;
  width: 350px;
  height: 130px;
  max-width: 350px;
  border-left: 6px solid #2dadf6;
  margin-bottom: 15px;

  @media (max-width: 1024px) {
    margin-right: 15px;
  }

  ${({ highPriority }) => highPriority && `border-left-color: #f97c1b;`}

  ${({ canStart }) =>
    canStart &&
    `
    border-top: 3px solid #57d6be;
    border-right: 3px solid #57d6be;
    border-bottom: 3px solid #57d6be;
  `}
`;

const Title = styled.div`
  font-weight: bold;
  font-size: 16px;
  margin-bottom: 6px;
`;

const Description = styled.div`
  font-size: 14px;
  max-width: 100%;
  max-height: 16px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

const CardBody = styled.div`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`;

const Next = styled.div`
  font-size: 14px;
  margin-bottom: 2px;
  color: rgba(255, 255, 255, 0.5);
`;

const Invites = styled.div`
  margin-top: 20px;
  display: flex;
`;

const StartButton = styled(Button)`
  background: #57d6be;
  padding: 2px 30px;
  margin-top: 3px;
  line-height: 0.8;
`;

export default Requests;
