import { FunctionComponent, useState } from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import { AppState } from '@/store';

import CreateRequestDialog from '@/components/CreateRequestDialog';
import Button from '@/components/shared/Button';
import RequestCard from '@/components/RequestCard';

const Requests: FunctionComponent = () => {
  const { requests, users } = useSelector((state: AppState) => state);
  const [showCreateRequestDialog, setShowCreateRequestDialog] = useState(false);

  return (
    <>
      {showCreateRequestDialog && (
        <CreateRequestDialog
          users={users}
          onDismiss={() => setShowCreateRequestDialog(false)}
        />
      )}
      <Wrapper>
        <Header>
          <Title>Task list</Title>
          <CreateButton onClick={() => setShowCreateRequestDialog(true)}>
            Create
          </CreateButton>
        </Header>

        <RequestsWrapper>
          {requests.map((request) => (
            <RequestCard key={request.id} request={request} />
          ))}
        </RequestsWrapper>
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  flex: 1;
  border-radius: 20px;
  background: #3d3c40;
  padding: 30px;

  @media (max-width: 1024px) {
    margin-bottom: 30px;
  }
`;

const RequestsWrapper = styled.div`
  height: calc(100vh - 280px);
  overflow-y: scroll;

  @media (max-width: 1024px) {
    height: 300px;
  }
`;

const CreateButton = styled(Button)`
  background: #2dadf6;
  height: 40px;
  padding: 0 20px;
  font-weight: bold;
  font-size: 16px;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 30px;
`;

const Title = styled.h2`
  margin: 0;
  font-weight: normal;
  font-size: 24px;
`;

export default Requests;
