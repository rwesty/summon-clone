import * as React from 'react';
import styled from 'styled-components';
import Link from 'next/link';
import { useDispatch, useSelector } from 'react-redux';
import { useCookies } from 'react-cookie';

import { AppState } from '@/store';
import { useStoreActions } from '@/store/actions';

const Topbar: React.FunctionComponent = () => {
  const dispatch = useDispatch();
  const storeActions = useStoreActions(dispatch);
  const [, , removeCookie] = useCookies([`auth`]);
  const { user } = useSelector((state: AppState) => state);

  return (
    <Wrapper>
      <Title>Summon</Title>
      <Profile>
        <ProfileText>
          {user && <ProfileName>Hi, {user.name}.</ProfileName>}
          <Logout
            onClick={() => {
              removeCookie(`auth`);
              storeActions.logout();
            }}
          >
            {user && <Link href="/login">Logout</Link>}
          </Logout>
        </ProfileText>
        {user && <ProfileImage image={`/img/${user.icon}`} />}
      </Profile>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: space-between;
  color: white;

  margin: 30px 0;
  padding: 0 60px;
`;

const Title = styled.h1`
  font-size: 24px;
  color: white;
  text-transform: uppercase;
`;

const Profile = styled.div`
  display: flex;
  align-items: center;
`;

const ProfileText = styled.div`
  margin-right: 10px;
  text-align: right;
`;

const ProfileName = styled.div`
  font-size: 18px;
  font-weight: bold;
`;

const Logout = styled.div`
  display: block;
  text-decoration: underline;
  font-size: 14px;
  margin-top: 3px;
  color: #bbb;
`;

const ProfileImage = styled.img<{ image: string }>`
  height: 60px;
  width: 60px;
  border-radius: 50%;

  background: url(${({ image }) => image}) center center/cover;
`;

export default Topbar;
