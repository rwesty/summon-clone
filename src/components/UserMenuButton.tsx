import { FunctionComponent, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Menu, MenuButton, MenuList, MenuItem } from '@reach/menu-button';

import { AppState } from '@/store';
import { User } from '@/models/types';

import Avatar from '@/components/shared/Avatar';
import '@reach/menu-button/styles.css';
import { useSelector } from 'react-redux';

type Props = {
  users: User[];
  onChange: (selectedUserList: User[]) => void;
};

const Combobox: FunctionComponent<Props> = ({ users, onChange, ...rest }) => {
  const { user: currentUser } = useSelector((state: AppState) => state);
  const [selectedUserList, setSelectedUserList] = useState<User[]>([]);
  const [userList, setUserList] = useState<User[]>([]);

  useEffect(() => {
    setUserList(users.filter((user) => user.id !== currentUser.id));
  }, []);

  return (
    <Menu>
      <Selected>
        {selectedUserList.map((user) => (
          <SelectedUser key={user.id}>
            <Avatar src={`/img/${user.icon}`} /> <span>{user.name}</span>
          </SelectedUser>
        ))}
        <StyledMenuButton disabled={!userList.length} {...rest}>
          <i className="fa fa-plus" />
        </StyledMenuButton>
        <StyledList>
          {userList.map((user) => (
            <StyledMenuItem
              onSelect={() => {
                const selectedUser = userList.find((u) => u.id === user.id);
                setUserList(userList.filter((u) => u.id !== selectedUser.id));
                const newSelected = [...selectedUserList, selectedUser];
                setSelectedUserList(newSelected);
                onChange(newSelected);
              }}
              key={user.id}
            >
              <Avatar src={`/img/${user.icon}`} /> <span>{user.name}</span>
            </StyledMenuItem>
          ))}
        </StyledList>
      </Selected>
    </Menu>
  );
};
const StyledMenuButton = styled(MenuButton)`
  border-radius: 50%;
  background: #555459;
  border: none;
  width: 30px;
  height: 30px;
  color: rgba(255, 255, 255, 0.5);
  box-shadow: 1px 2px 4px -2px black;
`;

const StyledList = styled(MenuList)`
  background: #3d3c40;
`;

const StyledMenuItem = styled(MenuItem)`
  display: flex;
  align-items: center;
  min-width: 200px;

  span {
    margin-left: 15px;
  }
`;

const Selected = styled.div`
  width: 100%;
  margin-top: 10px;
  display: flex;
  align-items: center;
`;

const SelectedUser = styled.div`
  display: flex;
  border-radius: 50px;
  background: #2dadf6;
  padding: 4px;
  padding-right: 10px;
  align-items: center;
  margin-right: 10px;
  font-size: 14px;

  div {
    ::before {
      content: none;
    }
  }

  img {
    width: 25px;
    height: 25px;
    margin-bottom: -2px;
  }

  span {
    margin-top: -2px;
    margin-left: 3px;
  }
`;

export default Combobox;
