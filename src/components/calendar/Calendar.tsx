import { FunctionComponent, useEffect } from 'react';
import { format, parse, startOfWeek, getDay } from 'date-fns';
import {
  Calendar as ReactBigCalendar,
  dateFnsLocalizer,
  CalendarProps,
} from 'react-big-calendar';
import styled from 'styled-components';

const locales = {
  'en-US': import(`date-fns/locale/en-US`),
};
const localizer = dateFnsLocalizer({
  format,
  parse,
  startOfWeek,
  getDay,
  locales,
});

const BigCalendar: FunctionComponent<Omit<CalendarProps, 'localizer'>> = ({
  ...rest
}) => {
  useEffect(() => {
    const sixAM = document.querySelector(`.rbc-timeslot-group:nth-child(7)`);

    if (sixAM) {
      sixAM.scrollIntoView();
    }
  }, []);

  return (
    <Wrapper>
      <ReactBigCalendar
        localizer={localizer}
        startAccessor="start"
        endAccessor="end"
        defaultView="week"
        formats={{
          timeGutterFormat: (date, culture) =>
            localizer.format(date, `h a`, culture),
        }}
        selectable
        step={60}
        timeslots={1}
        {...rest}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  height: calc(100vh - 210px);
  overflow-y: scroll;

  .rbc-time-view {
    border: none;
    margin-top: 80px;
  }

  .rbc-toolbar {
    > .rbc-btn-group:nth-child(3) {
      display: none;
    }
    .rbc-toolbar-label {
      display: none;
    }
  }

  .rbc-time-header-content {
    box-shadow: 0 8px 8px -8px rgba(0, 0, 0, 0.2);
  }

  .rbc-today {
    background: none;
  }
  .rbc-header .rbc-today {
    background: none;
  }

  .rbc-time-content {
    border-top: none;
  }

  .rbc-label {
    font-size: 14px;
  }

  .rbc-header,
  .rbc-timeslot-group {
    border-bottom: none;
  }
  .rbc-time-header-content,
  .rbc-day-bg + .rbc-day-bg,
  .rbc-header + .rbc-header,
  .rbc-time-header.rbc-overflowing {
    border: none;
  }

  .rbc-time-content > * + * > * {
    border-left: 1px solid rgba(255, 255, 255, 0.1);
  }

  .rbc-day-slot .rbc-time-slot {
    border-top: 1px solid rgba(255, 255, 255, 0.3);
  }

  .rbc-header {
    min-height: 70px;
  }

  .rbc-row-content {
    display: none;
  }

  .rbc-day-slot .rbc-event {
    background: #2dadf6;
    border: 0;
  }

  .rbc-day-slot .high-priority {
    border-left: 5px solid #ff8527;
  }

  .rbc-slot-selection {
    background: rgba(45, 173, 246, 0.5);
  }
`;

export default BigCalendar;
