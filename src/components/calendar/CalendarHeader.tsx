import { FunctionComponent } from 'react';
import styled from 'styled-components';
import { HeaderProps } from 'react-big-calendar';
import { isToday } from 'date-fns';

type Props = HeaderProps;

const CalendarHeader: FunctionComponent<Props> = ({ date, label }) => {
  const [dateNumber, day] = label.split(` `);
  const today = isToday(date);

  return (
    <>
      <Day today={today}>{day}</Day>
      <Date today={today}>{dateNumber}</Date>
    </>
  );
};

const Day = styled.div<{ today: boolean }>`
  font-size: 12px;
  font-weight: normal;
  text-transform: uppercase;

  ${({ today }) => today && `color: #57d6be`}
`;

const Date = styled.div<{ today: boolean }>`
  position: relative;
  font-size: 24px;
  font-weight: normal;
  margin-top: 8px;
  z-index: 1;

  ${({ today }) =>
    today &&
    `
    color: black;

    &::before {
      content: '';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      width: 40px;
      height: 40px;
      background: #57d6be;
      border-radius: 50%;
      z-index: -1;
    }
  `};
`;

export default CalendarHeader;
