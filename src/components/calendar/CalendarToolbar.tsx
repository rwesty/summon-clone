import { FunctionComponent } from 'react';
import styled from 'styled-components';
import { format } from 'date-fns';
import { ToolbarProps } from 'react-big-calendar';

import Button from '@/components/shared/Button';

type Props = ToolbarProps;

const CalendarToolbar: FunctionComponent<Props> = ({
  localizer,
  onNavigate,
}) => {
  const { messages } = localizer;

  return (
    <Wrapper>
      <MonthYear>{format(new Date(), `MMMM, yyyy`)}</MonthYear>
      <NavigationGroup>
        <DateButton type="button" onClick={() => onNavigate(`PREV`)}>
          <i className="fa fa-angle-left" />
        </DateButton>
        <DateButton type="button" onClick={() => onNavigate(`TODAY`)}>
          {messages.today}
        </DateButton>
        <DateButton type="button" onClick={() => onNavigate(`NEXT`)}>
          <i className="fa fa-angle-right" />
        </DateButton>
      </NavigationGroup>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: absolute;
  width: calc(100% - 60px);
  display: flex;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const MonthYear = styled.h2`
  font-size: 35px;
  font-weight: normal;
  margin: 0;
`;

const NavigationGroup = styled.div`
  display: flex;
  align-items: flex-end;
`;

const DateButton = styled(Button)`
  i {
    color: white;
    font-size: 20px;
    font-weight: bold;
  }
`;

export default CalendarToolbar;
