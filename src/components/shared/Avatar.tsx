import { FunctionComponent } from 'react';
import styled from 'styled-components';

type Props = {
  src: string;
  z?: number;
  stacked?: boolean;
} & React.HTMLProps<HTMLImageElement>;

const Avatar: FunctionComponent<Props> = ({ src, z, stacked, ...rest }) => (
  <AvatarWrapper stacked={stacked} z={z}>
    <AvatarImage src={src} {...rest} />
  </AvatarWrapper>
);

const AvatarWrapper = styled.div<{ z: number; stacked: boolean }>`
  position: relative;
  width: 30px;
  z-index: 1;
  flex-direction: row-reverse;

  ${({ z, stacked }) =>
    stacked &&
    `
      z-index: ${100 - z};
      margin-right: -8px;
  `}

  &::before {
    content: '';
    position: absolute;
    top: -2px;
    left: -2px;
    width: calc(100% + 4px);
    height: calc(100% + 1px);
    background: white;
    border-radius: 50%;
    z-index: -1;
  }
`;

const AvatarImage = styled.img`
  width: 30px;
  height: 30px;
  object-fit: cover;
  border-radius: 50%;
`;

export default Avatar;
