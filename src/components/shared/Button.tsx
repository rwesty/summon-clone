import styled from 'styled-components';

const Button = styled.button`
  cursor: pointer;
  background: #555459;
  color: white;
  border: none;
  border-radius: 5px;
  height: 30px;
  margin: 0 2px;
  line-height: 30px;
  box-shadow: 1px 2px 4px -2px black;
  font-size: 18px;
`;

export default Button;
