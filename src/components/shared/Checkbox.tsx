import React, { FunctionComponent, Ref } from 'react';
import styled, { StyledComponentProps } from 'styled-components';

type CheckboxProps = {
  label: string;
  subLabel?: string;
  // Apply classname to wrapper component, since input is visually hidden
  className?: string;
  ref?: Ref<HTMLInputElement>;
};

const Checkbox: FunctionComponent<
  StyledComponentProps<'input', any, CheckboxProps, never>
> = React.forwardRef(({ id, label, subLabel, className, ...rest }, ref) => (
  <CheckboxWrapper className={className}>
    <VisuallyHidden as="input" id={id} type="checkbox" ref={ref} {...rest} />
    <CheckboxLabel htmlFor={id}>
      <PseudoCheckbox>
        <CheckboxIcon className="fa fa-check" />
      </PseudoCheckbox>
      <Label>
        {label}
        {subLabel && <SubLabel>{subLabel}</SubLabel>}
      </Label>
    </CheckboxLabel>
  </CheckboxWrapper>
));

export default Checkbox;

const CheckboxWrapper = styled.div`
  border-top: 1px solid #555459;
  border-bottom: 1px solid #555459;
  padding: 30px 0;

  /* https://css-tricks.com/the-checkbox-hack/
    Cross browser compatibility is achieved by hiding the default
    checkbox and using CSS to style a div based on its current value */
  input[type='checkbox']:checked ~ label div::before {
    background: #57d6be;
  }

  input[type='checkbox']:focus ~ label {
    outline-width: 2px;
    outline-style: solid;
    /* Replicate focus styles */
    outline-color: Highlight;
    outline-color: -webkit-focus-ring-color;
  }
`;

const CheckboxLabel = styled.label`
  cursor: pointer;

  display: flex;
`;

const Label = styled.div`
  margin-left: 5px;
  font-size: 16px;
  margin-top: -2px;
`;

const SubLabel = styled.div`
  font-size: 14px;
  color: rgba(255, 255, 255, 0.5);
  margin-top: 5px;
`;

const PseudoCheckbox = styled.div`
  position: relative;
  height: 24px;
  flex: 0 0 24px;
  border-radius: 5px;
  border: 1px solid #aaaaaa;
  /* Align checkbox to middle of top line */
  margin-top: -2px;
  margin-right: 8px;
  background: #3d3c40;
  display: flex;
  justify-content: center;
  align-items: center;

  &::before {
    position: absolute;
    content: '';
    width: 24px;
    height: 24px;
    border-radius: 5px;
  }
`;

const CheckboxIcon = styled.i`
  z-index: 1;
  position: relative;
  font-size: 12px;
  color: #3d3c40;
  padding-top: 2px;
`;

const VisuallyHidden = styled.div`
  clip: rect(0 0 0 0);
  overflow: hidden;
  position: absolute;
  height: 1px;
  width: 1px;
`;
