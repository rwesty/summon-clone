import { FunctionComponent } from 'react';
import styled from 'styled-components';
import { Dialog as ReachDialog, DialogProps } from '@reach/dialog';

import '@reach/dialog/styles.css';

type Props = DialogProps;

const Dialog: FunctionComponent<Props> = ({ children, ...rest }) => (
  <StyledDialog {...rest}>{children}</StyledDialog>
);

const StyledDialog = styled(ReachDialog)`
  &[data-reach-dialog-content] {
    border-radius: 20px;
    background: #3d3c40;
    padding: 0;
    width: 100vw;
    max-width: 400px;
  }

  @media (max-width: 480px) {
    &[data-reach-dialog-content] {
      border-radius: 0;
      height: 100vh;
      max-width: 100%;
      margin: 0;
    }
  }
`;

export default Dialog;
