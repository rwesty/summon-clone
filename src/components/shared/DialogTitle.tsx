import styled from 'styled-components';

const DialogTitle = styled.h1`
  margin: 0;
  font-size: 24px;
  font-weight: normal;
  margin-bottom: 15px;
`;

export default DialogTitle;
