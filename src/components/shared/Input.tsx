import styled from 'styled-components';

const Input = styled.input`
  background: none;
  color: white;
  border: none;
  height: 45px;
  width: 100%;
  font-size: 18px;
  padding: 0 10px;

  ::placeholder {
    font-size: 20px;
    color: rgba(255, 255, 255, 0.5);
  }
`;

export default Input;
