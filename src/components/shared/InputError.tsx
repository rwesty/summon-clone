import styled from 'styled-components';

const Input = styled.div`
  color: #f9381c;
  position: absolute;
  top: 0;
  right: 0;
  font-size: 14px;
`;

export default Input;
