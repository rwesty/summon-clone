import styled from 'styled-components';

const Input = styled.div`
  position: relative;
  border-bottom: 1px solid #555459;
  height: 45px;
  margin: 15px 0 0 0;
`;

export default Input;
