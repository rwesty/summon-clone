import styled from 'styled-components';

const Label = styled.div`
  font-size: 18px;
  margin: 15px 0;
`;

export default Label;
