import { PrismaClient } from '@prisma/client';

const globalAny: any = global;
let prisma: PrismaClient; // eslint-disable-line

if (process.env.NODE_ENV !== `production`) {
  if (!globalAny.prisma) {
    globalAny.prisma = new PrismaClient();
  }
  prisma = globalAny.prisma;
} else {
  prisma = new PrismaClient();
}

export default prisma;
