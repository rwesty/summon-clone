import { TimeSlot } from '@prisma/client';
import { Request } from '@/models/types';
import { isAfter, isBefore, isEqual } from 'date-fns';

function hasOverlap(a: TimeSlot, b: TimeSlot) {
  if (isEqual(a.startTime, b.startTime) && isEqual(a.endTime, b.endTime)) {
    return true;
  }

  return isBefore(a.startTime, b.endTime) && isAfter(a.endTime, b.startTime);
}

// Given an array of slots return the earliest a meeting could start and the latest it can start
function startAndEndTime(slots: TimeSlot[]): [Date, Date] {
  const start = slots.reduce((prev, curr) => {
    if (curr.startTime.getHours() > prev.startTime.getHours()) {
      return curr;
    }
    return prev;
  }, slots[0]).startTime;

  const end = slots.reduce((prev, curr) => {
    if (curr.endTime.getHours() < prev.endTime.getHours()) {
      return curr;
    }
    return prev;
  }, slots[0]).endTime;

  return [start, end];
}

/*
  Get first overlapping timeslot for a set of users
*/
export const getNextAvailableStartTime = async (
  request: Omit<Request, 'nextAvailableStartTime' | 'canStart'>,
  slotsByUser: TimeSlot[][],
): Promise<Date[]> => {
  const inviteSlots = request.invites.map(
    (user) => slotsByUser.find((slot) => slot[0]?.userId === user.id) || [],
  );
  const userSlots =
    slotsByUser.find((slots) => slots[0]?.userId === request.userId) || [];

  for (const desiredSlot of userSlots) {
    const searched = [];

    for (const slots of inviteSlots) {
      searched.push(binarySearchMatches(desiredSlot, slots));
    }

    if (searched.length === inviteSlots.length && !searched.includes(null)) {
      return startAndEndTime([desiredSlot, ...searched]);
    }
  }

  // No available time to meet!
  return [];
};

function binarySearchMatches(
  desiredSlot: TimeSlot,
  slots: TimeSlot[],
): TimeSlot | null {
  const mid = Math.floor(slots.length / 2);
  const compareSlot = slots[mid];

  if (compareSlot && hasOverlap(desiredSlot, compareSlot)) {
    return compareSlot;
  }

  if (slots.length === 1) {
    return null;
  }

  if (isBefore(desiredSlot.startTime, slots[mid].startTime)) {
    return binarySearchMatches(desiredSlot, slots.slice(0, mid));
  } else {
    return binarySearchMatches(desiredSlot, slots.slice(mid));
  }
}
