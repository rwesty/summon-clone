import prisma from '@/lib/prisma';
import { Prisma, Request, User, TimeSlot } from '@prisma/client';
import { isWithinInterval, isBefore } from 'date-fns';

import { Request as UserRequest } from '@/models/types';
import { getNextAvailableStartTime } from '@/models/requests/getNextAvailableStartTime';
import { getSlotsByUser } from '../timeSlots';

export const sortAndSerializeRequests = async (
  requests: (Request & { invites: User[] })[],
  slotsByUser: TimeSlot[][],
) => {
  const transformed = await Promise.all(
    requests.map((request) => transformToUserRequest(request, slotsByUser)),
  );

  // Sort by next available and highPriority
  transformed.sort((a, b) => {
    if (a.nextAvailableStartTime && !b.nextAvailableStartTime) {
      return -1;
    } else if (b.nextAvailableStartTime && !a.nextAvailableStartTime) {
      return 1;
    }

    if (isBefore(a.nextAvailableStartTime, b.nextAvailableStartTime)) {
      return -1;
    } else if (isBefore(b.nextAvailableStartTime, a.nextAvailableStartTime)) {
      return 1;
    }

    if (a.highPriority && !b.highPriority) {
      return -1;
    } else if (b.highPriority && !a.highPriority) {
      return 1;
    }

    return 0;
  });

  return transformed.map((r) => ({
    ...r,
    nextAvailableStartTime: r.nextAvailableStartTime
      ? r.nextAvailableStartTime.toISOString()
      : null,
  }));
};

type TransformReturn = Omit<UserRequest, 'nextAvailableStartTime'> & {
  nextAvailableStartTime: Date | null;
};

export const transformToUserRequest = async (
  request: Request & { invites: User[] },
  slotsByUser: TimeSlot[][],
): Promise<TransformReturn> => {
  const [start, end] = await getNextAvailableStartTime(request, slotsByUser);

  return {
    ...request,
    nextAvailableStartTime: start ? start : null,
    canStart: start ? isWithinInterval(new Date(), { start, end }) : false,
  };
};

export const getUserRequests = async (id: number) => {
  const user = await prisma.user.findUnique({
    where: {
      id,
    },
    include: {
      requests: {
        include: {
          invites: true,
        },
      },
      invites: {
        include: {
          invites: true,
        },
      },
    },
  });

  const slotsByUser = await getSlotsByUser();

  return sortAndSerializeRequests(
    [...user.requests, ...user.invites],
    slotsByUser,
  );
};

export const createRequest = async (payload: Prisma.RequestCreateInput) => {
  const request = await prisma.request.create({
    data: payload,
    include: {
      invites: true,
    },
  });

  const slotsByUser = await getSlotsByUser();

  return transformToUserRequest(request, slotsByUser);
};
