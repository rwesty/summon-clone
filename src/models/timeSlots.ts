import prisma from '@/lib/prisma';
import { Prisma } from '@prisma/client';

import { getUsers } from './user';

export const createTimeSlot = async (payload: Prisma.TimeSlotCreateInput) => {
  const timeSlot = await prisma.timeSlot.create({
    data: payload,
  });

  return timeSlot;
};

export const getSlotsByUser = async () => {
  const users = await getUsers();

  return Promise.all(
    users.map((user) =>
      prisma.timeSlot.findMany({
        where: {
          userId: {
            equals: user.id,
          },
          endTime: {
            gte: new Date(),
          },
        },
        orderBy: {
          startTime: `asc`,
        },
      }),
    ),
  );
};
