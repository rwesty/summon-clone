import {
  User as UserType,
  TimeSlot as TimeSlotType,
  Request as RequestType,
} from '@prisma/client';

export type { User } from '@prisma/client';

export type Request = RequestType & {
  invites: UserType[];
  nextAvailableStartTime: string | null;
  canStart: boolean;
};

export type TimeSlot = TimeSlotType & {
  startTime: string;
  endTime: string;
};

export type UserContext = {
  user: UserType;
  users: UserType[];
  timeSlots: TimeSlot[];
  requests: Request[];
};
