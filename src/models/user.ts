import prisma from '@/lib/prisma';

import { TimeSlot, User, UserContext } from '@/models/types';
import { sortAndSerializeRequests } from './requests';
import { getSlotsByUser } from './timeSlots';

export const getUsers = async (): Promise<User[]> => prisma.user.findMany();

export const getCurrentUserContext = async (
  userId: number,
): Promise<UserContext> => {
  const user = await prisma.user.findUnique({
    where: {
      id: userId,
    },
    include: {
      timeSlots: true,
      requests: {
        include: {
          invites: true,
        },
      },
      invites: {
        include: {
          invites: true,
        },
      },
    },
  });

  const [users, slotsByUser] = await Promise.all([
    getUsers(),
    getSlotsByUser(),
  ]);

  return {
    user: {
      id: user.id,
      name: user.name,
      icon: user.icon,
    },
    users: users.filter((u) => u.id !== userId),
    timeSlots: user.timeSlots.map(
      (timeSlot) =>
        ({
          ...timeSlot,
          startTime: timeSlot.startTime.toISOString(),
          endTime: timeSlot.endTime.toISOString(),
        } as TimeSlot),
    ),
    requests: await sortAndSerializeRequests(
      [...user.requests, ...user.invites],
      slotsByUser,
    ),
  };
};
