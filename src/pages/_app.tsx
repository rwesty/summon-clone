import { Provider } from 'react-redux';
import { createWrapper } from 'next-redux-wrapper';

import { configureStore } from '@/store/index';

import '@/styles/global.scss';
import 'react-big-calendar/lib/sass/styles.scss';

const store = configureStore();
const makeStore = () => store;

const wrapper = createWrapper(makeStore);

export default wrapper.withRedux(({ Component, pageProps }) => (
  <Provider store={store}>
    <Component {...pageProps} />
  </Provider>
));
