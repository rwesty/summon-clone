import { NextApiRequest, NextApiResponse } from 'next';

import pusher from '@/services/pusher';
import { createRequest } from '@/models/requests/index';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const {
    query: { id },
    body: { title, description, highPriority, invites },
    method,
  } = req;

  switch (method) {
    case `POST`:
      const request = await createRequest({
        creator: {
          connect: {
            id: parseInt(id as string, 10),
          },
        },
        title,
        description,
        highPriority,
        invites: {
          connect: invites,
        },
      });

      res.json(request);

      await pusher.trigger(`app`, `refresh:requests`, {});
      break;
    default:
      res.status(404).send(``);
  }
}
