import { NextApiRequest, NextApiResponse } from 'next';

import { getUserRequests } from '@/models/requests/index';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const {
    query: { id },
    method,
  } = req;

  switch (method) {
    case `GET`:
      const requests = await getUserRequests(parseInt(id as string, 10));

      res.json(requests);
      break;
    default:
      res.status(404).send(``);
  }
}
