import { NextApiRequest, NextApiResponse } from 'next';

import pusher from '@/services/pusher';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const {
    query: { id },
    body: { requestId },
    method,
  } = req;

  switch (method) {
    case `POST`:
      const userId = parseInt(id as string, 10);

      if (userId && requestId) {
        await pusher.trigger(`app`, `session:started`, {
          userId,
          requestId,
        });
        res.status(200).end();
        break;
      }

      res.status(400).end();
      break;
    default:
      res.status(404).send(``);
  }
}
