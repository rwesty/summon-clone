import { NextApiRequest, NextApiResponse } from 'next';

import { createTimeSlot } from '@/models/timeSlots';

import pusher from '@/services/pusher';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const {
    query: { id },
    body: { startTime, endTime, highPriority },
    method,
  } = req;

  switch (method) {
    case `POST`:
      const userId = parseInt(id as string, 10);
      const timeSlot = await createTimeSlot({
        user: {
          connect: {
            id: userId,
          },
        },
        startTime,
        endTime,
        highPriority,
      });

      res.json(timeSlot);

      await pusher.trigger(`app`, `refresh:requests`, {});
      break;
    default:
      res.status(404).send(``);
  }
}
