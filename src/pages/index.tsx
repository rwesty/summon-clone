import React, { FunctionComponent, useEffect } from 'react';
import styled from 'styled-components';
import { GetServerSideProps } from 'next';
import { useDispatch } from 'react-redux';
import cookie from 'cookie';

import { getCurrentUserContext } from '@/models/user';
import { UserContext } from '@/models/types';
import Topbar from '@/components/Topbar';
import CalendarView from '@/components/CalendarView';
import Requests from '@/components/Requests';
import { useStoreActions } from '@/store/actions';

type Props = {
  userContext: UserContext;
};

export const getServerSideProps: GetServerSideProps<
  Props | Record<string, unknown>
> = async ({ req, res }) => {
  const cookies = cookie.parse(
    req ? req.headers.cookie || `` : document.cookie,
  );
  const userId = parseInt(cookies.user, 10);

  if (!userId) {
    res.setHeader(`location`, `/login`);
    res.statusCode = 302;
    res.end();
    return { props: {} };
  }

  const userContext = await getCurrentUserContext(userId);
  return { props: { userContext } };
};

const App: FunctionComponent<Props> = ({ userContext }) => {
  const dispatch = useDispatch();
  const userActions = useStoreActions(dispatch);

  useEffect(() => {
    userActions.userContext(userContext);
    userActions.setupPusher();
  }, []);

  return (
    <AppWrapper>
      <Topbar />
      <Dashboard>
        <CalendarView />
        <Requests />
      </Dashboard>
    </AppWrapper>
  );
};

const AppWrapper = styled.div`
  height: 100vh;
  overflow: hidden;

  @media (max-width: 1024px) {
    height: unset;
  }
`;

const Dashboard = styled.div`
  display: flex;
  padding: 0 60px;

  @media (max-width: 1024px) {
    flex-direction: column-reverse;
  }

  @media (max-width: 1024px) {
    padding: 0 30px;
  }
`;

export default App;
