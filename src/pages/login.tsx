import { FunctionComponent } from 'react';
import { useCookies } from 'react-cookie';
import styled from 'styled-components';
import { GetStaticProps } from 'next';
import Router from 'next/router';

import Topbar from '@/components/Topbar';
import { User } from '@/models/types';
import { getUsers } from '@/models/user';

type Props = {
  users: User[];
};

export const getStaticProps: GetStaticProps<Props> = async () => {
  const users = await getUsers();
  return { props: { users } };
};

const Login: FunctionComponent<Props> = ({ users }) => {
  const [, setCookie] = useCookies([`auth`]);

  return (
    <>
      <Topbar />
      <Users>
        <Title>Choose your login.</Title>
        {users.map((user) => (
          <UserLogin
            key={user.id}
            onClick={() => {
              setCookie(`user`, user.id, {
                path: `/`,
                maxAge: 600,
              });
              Router.push(`/`);
            }}
          >
            <Avatar src={`/img/${user.icon}`} />
            <UserText>{user.name}</UserText>
          </UserLogin>
        ))}
      </Users>
    </>
  );
};

const Users = styled.div`
  display: flex;
  width: fit-content;
  margin: 0 auto;
  margin-top: 100px;
  flex-wrap: wrap;
  justify-content: center;
  text-align: center;
`;

const Title = styled.h1`
  font-size: 40px;
  width: 100%;
  margin-bottom: 60px;
`;

const UserLogin = styled.button`
  cursor: pointer;
  background: none;
  border: none;
  color: white;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 0 30px;
`;

const Avatar = styled.img`
  width: 200px;
  height: 200px;
  border-radius: 50%;
  object-fit: cover;
`;

const UserText = styled.div`
  font-size: 30px;
  margin-top: 15px;
`;

export default Login;
