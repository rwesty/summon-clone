#!/usr/bin/env node

const { PrismaClient } = require('@prisma/client');
const { lastDayOfWeek, subWeeks, addDays } = require('date-fns');

const prisma = new PrismaClient();

function getDatePlusHours(hours, dayOfTheWeek) {
  // Start on last sunday, add number of days, eg. Monday = 1
  const d = addDays(
    subWeeks(lastDayOfWeek(new Date(), { weekStartsOn: 1 }), 1),
    dayOfTheWeek,
  );
  d.setHours(hours, 0, 0, 0);
  return new Date(d);
}

const seed = async () => {
  const users = [
    { name: `Ivan`, icon: `ivan.jpg` },
    { name: `Yvonne`, icon: `yvonne.jpg` },
    { name: `Simon`, icon: `simon.jpg` },
    { name: `Michael`, icon: `michael.jpg` },
  ];

  for (const user of users) {
    await prisma.user.upsert({
      create: user,
      update: user,
      where: {
        name: user.name,
      },
    });
  }

  const timeSlots = [
    // Ivan
    {
      startTime: getDatePlusHours(7, 6),
      endTime: getDatePlusHours(11, 6),
      highPriority: false,
      user: {
        connect: {
          id: 1,
        },
      },
    },
    {
      startTime: getDatePlusHours(7, 5),
      endTime: getDatePlusHours(11, 5),
      highPriority: false,
      user: {
        connect: {
          id: 1,
        },
      },
    },
    {
      startTime: getDatePlusHours(4, 4),
      endTime: getDatePlusHours(8, 4),
      highPriority: true,
      user: {
        connect: {
          id: 1,
        },
      },
    },
    // Simon
    {
      startTime: getDatePlusHours(6, 5),
      endTime: getDatePlusHours(8, 5),
      highPriority: false,
      user: {
        connect: {
          id: 2,
        },
      },
    },
    {
      startTime: getDatePlusHours(6, 6),
      endTime: getDatePlusHours(8, 6),
      highPriority: false,
      user: {
        connect: {
          id: 2,
        },
      },
    },
    {
      startTime: getDatePlusHours(12, 4),
      endTime: getDatePlusHours(15, 4),
      highPriority: true,
      user: {
        connect: {
          id: 2,
        },
      },
    },
    // Michael
    {
      startTime: getDatePlusHours(7, 5),
      endTime: getDatePlusHours(9, 5),
      highPriority: false,
      user: {
        connect: {
          id: 3,
        },
      },
    },
    {
      startTime: getDatePlusHours(7, 5),
      endTime: getDatePlusHours(9, 5),
      highPriority: false,
      user: {
        connect: {
          id: 3,
        },
      },
    },
    {
      startTime: getDatePlusHours(14, 4),
      endTime: getDatePlusHours(16, 4),
      highPriority: true,
      user: {
        connect: {
          id: 3,
        },
      },
    },
  ];

  for (const [i, slot] of timeSlots.entries()) {
    await prisma.timeSlot.upsert({
      create: slot,
      update: slot,
      where: {
        id: i + 1,
      },
    });
  }

  const requests = [
    {
      title: `Touchbase`,
      description: `Just a catchup.`,
      highPriority: false,
      creator: { connect: { id: 1 } },
      invites: { connect: [{ id: 2 }, { id: 3 }] },
    },
  ];

  for (const [i, request] of requests.entries()) {
    await prisma.request.upsert({
      create: request,
      update: request,
      where: {
        id: i + 1,
      },
    });
  }
};

seed()
  .catch((err) => {
    throw err;
  })
  .finally(() => {
    prisma.$disconnect();
  });
