import superagent, { SuperAgentRequest } from 'superagent';

const prefix = (base: string | undefined) => (request: SuperAgentRequest) => {
  request.url = base + request.url;
};

export default superagent
  .agent()
  .use(prefix(`/api`))
  .accept(`application/json`);
