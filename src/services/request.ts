import api from '@/services/api';
import { Prisma } from '@prisma/client';

import { Request } from '@/models/types';

export type CreateRequestPayload = Prisma.RequestCreateInput & {
  creator: number;
};

export const createRequest = async (
  payload: CreateRequestPayload,
): Promise<Request> => {
  const response = await api
    .post(`/user/${payload.creator}/request`)
    .send(payload);

  return response.body;
};

export const getUserRequests = async (id: number): Promise<Request[]> => {
  const response = await api.get(`/user/${id}/requests`);

  return response.body;
};

export const saveSessionResponse = async (
  userId: number,
  requestId: number,
  response: string,
) => {
  await api.post(`/user/${userId}/sessionResponse`).send({
    requestId,
    response,
  });
};

export const startSession = async (userId, requestId) => {
  await api.post(`/user/${userId}/startSession`).send({
    requestId,
  });
};
