import api from '@/services/api';
import { Prisma } from '@prisma/client';

import { TimeSlot } from '@/models/types';

export type CreateTimeSlotPayload = Prisma.TimeSlotCreateInput & {
  user: number;
};

export const createTimeSlot = async (
  payload: CreateTimeSlotPayload,
): Promise<TimeSlot> => {
  const response = await api
    .post(`/user/${payload.user}/timeSlot`)
    .send(payload);

  return response.body;
};
