import { bindActionCreators, Dispatch } from 'redux';
import Pusher from 'pusher-js';
import * as PusherTypes from 'pusher-js';
import { ThunkDispatch } from 'redux-thunk';

import { UserContext } from '@/models/types';
import {
  CREATE_REQUEST,
  USER_CONTEXT,
  CREATE_TIMESLOT,
  REFRESH_REQUESTS,
  StoreActions,
  LOGOUT,
  SESSION_STARTED,
  SessionResponsePayload,
  SESSION_RESPONSE,
} from '@/store/types';
import {
  createRequest as createRequestService,
  CreateRequestPayload,
  getUserRequests,
} from '@/services/request';
import {
  createTimeSlot as createTimeSlotService,
  CreateTimeSlotPayload,
} from '@/services/timeSlot';
import { AppState } from './index';

const setupPusher = () => (
  dispatch: ThunkDispatch<AppState, null, StoreActions>,
) => {
  const pusher = new Pusher(process.env.NEXT_PUBLIC_PUSHER_KEY, {
    cluster: process.env.NEXT_PUBLIC_PUSHER_CLUSTER,
  });

  const channel: PusherTypes.Channel = pusher.subscribe(`app`);

  channel.bind(`refresh:requests`, () => {
    dispatch(refreshRequests());
  });

  channel.bind(`session:started`, (data) => {
    dispatch(sessionStarted(data));
  });

  channel.bind(`session:response`, (data) => {
    dispatch(sessionResponse(data));
  });
};

const userContext = (context: UserContext) => (
  dispatch: Dispatch<StoreActions>,
) => {
  dispatch({
    type: USER_CONTEXT,
    payload: context,
  });
};

const sessionStarted = (data) => (
  dispatch: Dispatch<StoreActions>,
  getState: () => AppState,
) => {
  const { user } = getState();

  if (user.id === data.userId) {
    return;
  }

  dispatch({
    type: SESSION_STARTED,
    payload: data,
  });
};

const sessionResponse = (data: SessionResponsePayload) => (
  dispatch: Dispatch<StoreActions>,
) => {
  dispatch({
    type: SESSION_RESPONSE,
    payload: data,
  });
};

const createRequest = (payload: CreateRequestPayload) => async (
  dispatch: Dispatch<StoreActions>,
) => {
  const request = await createRequestService(payload);

  dispatch({
    type: CREATE_REQUEST,
    payload: request,
  });
};

const refreshRequests = () => async (
  dispatch: Dispatch<StoreActions>,
  getState: () => AppState,
) => {
  const { user } = getState();
  const requests = await getUserRequests(user.id);

  dispatch({
    type: REFRESH_REQUESTS,
    payload: requests,
  });
};

const createTimeSlot = (payload: CreateTimeSlotPayload) => async (
  dispatch: ThunkDispatch<AppState, null, StoreActions>,
) => {
  const request = await createTimeSlotService(payload);

  dispatch({
    type: CREATE_TIMESLOT,
    payload: request,
  });

  dispatch(refreshRequests());
};

const logout = () => (dispatch: Dispatch<StoreActions>) =>
  dispatch({ type: LOGOUT });

export const useStoreActions = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      userContext,
      setupPusher,
      createRequest,
      createTimeSlot,
      refreshRequests,
      logout,
    },
    dispatch,
  );
