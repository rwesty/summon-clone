import { Store, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { AppState as AppStateType, reducer } from '@/store/reducers';

export type AppState = AppStateType;

export function configureStore(initialState?: AppState): Store<AppState> {
  let middleware = applyMiddleware(thunk);

  if (process.env.NODE_ENV !== `production`) {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(
    reducer as any,
    initialState as any,
    middleware,
  ) as Store<AppState>;

  return store;
}
