import { Reducer } from 'redux';

import { TimeSlot, User, Request } from '@/models/types';
import {
  USER_CONTEXT,
  StoreActions,
  CREATE_REQUEST,
  CREATE_TIMESLOT,
  REFRESH_REQUESTS,
  LOGOUT,
  SESSION_RESPONSE,
  SESSION_STARTED,
} from '@/store/types';

export type InviteWithResponse = User & {
  response?: string;
};
export type RequestWithResponse = Omit<Request, 'invites'> & {
  invites: InviteWithResponse[];
  started?: boolean;
};
export type AppState = {
  user: User | null;
  requests: (Request & { invites: InviteWithResponse[] })[];
  users: User[];
  timeSlots: TimeSlot[];
};

const initialState: AppState = {
  user: null,
  requests: [],
  users: [],
  timeSlots: [],
};

export const reducer: Reducer<AppState, StoreActions> = (
  state = initialState,
  action: StoreActions,
): AppState => {
  switch (action.type) {
    case USER_CONTEXT:
      return {
        ...action.payload,
      };
    case CREATE_REQUEST:
      return {
        ...state,
        requests: [...state.requests, action.payload],
      };
    case CREATE_TIMESLOT:
      return {
        ...state,
        timeSlots: [...state.timeSlots, action.payload],
      };
    case REFRESH_REQUESTS:
      return {
        ...state,
        requests: action.payload,
      };
    case SESSION_STARTED:
      return {
        ...state,
        requests: state.requests.map((request) => {
          if (request.id === action.payload.requestId) {
            return {
              ...request,
              started: true,
            };
          }
          return request;
        }),
      };
    case SESSION_RESPONSE:
      return {
        ...state,
        requests: state.requests.map((request) => {
          if (request.id === action.payload.requestId) {
            const newInvites = request.invites.map((member) => {
              if (member.id === action.payload.userId) {
                return {
                  ...member,
                  response: action.payload.response,
                };
              }
              return member;
            });
            return {
              ...request,
              invites: newInvites,
            };
          }
          return request;
        }),
      };

    case LOGOUT:
      return initialState;
    default:
      return state;
  }
};
