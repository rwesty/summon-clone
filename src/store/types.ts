import { Request, TimeSlot, UserContext } from '@/models/types';

export const USER_CONTEXT = `USER_CONTEXT`;
export const CREATE_REQUEST = `CREATE_REQUEST`;
export const CREATE_TIMESLOT = `CREATE_TIMESLOT`;
export const REFRESH_REQUESTS = `REFRESH_REQUESTS`;
export const LOGOUT = `LOGOUT`;
export const SESSION_RESPONSE = `SESSION_RESPONSE`;
export const SESSION_STARTED = `SESSION_STARTED`;

export type SessionStartedPayload = {
  requestId: number;
};
export type SessionStartedAction = {
  type: typeof SESSION_STARTED;
  payload: SessionStartedPayload;
};

export type SessionResponsePayload = {
  userId: number;
  requestId: number;
  response: string;
};
export type SessionResponseAction = {
  type: typeof SESSION_RESPONSE;
  payload: SessionResponsePayload;
};

export type UserContextAction = {
  type: typeof USER_CONTEXT;
  payload: UserContext;
};

export type CreateRequestAction = {
  type: typeof CREATE_REQUEST;
  payload: Request;
};

export type CreateTimeSlotAction = {
  type: typeof CREATE_TIMESLOT;
  payload: TimeSlot;
};

export type RefreshRequestsAction = {
  type: typeof REFRESH_REQUESTS;
  payload: Request[];
};

export type LogoutAction = {
  type: typeof LOGOUT;
};

export type StoreActions =
  | UserContextAction
  | CreateRequestAction
  | CreateTimeSlotAction
  | RefreshRequestsAction
  | LogoutAction
  | SessionResponseAction
  | SessionStartedAction;
